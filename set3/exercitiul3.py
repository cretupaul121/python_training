# functie pentru inversarea unui string
# am ales sa fac reversul pe un string ca encodarea in baza 16 trebuie sa fie de tip string
# si pentru simplitate encodarea in baza 2 e tot de tip string

def revStr(s):
    j = len(s) -1
    reversedstring = ""

    while j >= 0:
        reversedstring += s[j]
        j -= 1
    return reversedstring

def toBinary(nr):
    binar = ""

    while nr > 0:
        rest = nr % 2
        binar += str(rest)
        nr = int(nr / 2)

    return revStr(binar)


def toHexa(nr):
    arr2 = ["A","B","C","D","E","F","G"]
    hexa = ""

    while nr != 0:
        rest = nr % 16

        if rest <= 9:
            hexa += str(rest)
        else:
            rest2 = rest % 10
            hexa += arr2[rest2]
        nr = int(nr / 16)

    return revStr(hexa)

def tupleList(ls):
    tp_list = []

    for i in ls:
        elem = (i,toBinary(i),toHexa(i))
        tp_list.append(elem)

    return tp_list

arr = [1,5,2,3,5,6,10,11,65,1000]

print(tupleList(arr))








