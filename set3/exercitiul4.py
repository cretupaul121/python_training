
def lambda_sort(tuples):
    tuples.sort(key=lambda x: x[1])

def count_sort(tuples):

    output = [0] * len(tuples)
    count = [0] * 20

    for i in range(0,len(tuples)):
        count[tuples[i][1]] += 1

    for i in range(1,10):
        count[i] += count[i - 1]

    i = len(tuples) - 1

    while i >= 0:
        output[count[tuples[i][1]] - 1] = tuples[i]
        count[tuples[i][1]] -= 1
        i -= 1

    for i in range(0,len(tuples)):
        tuples[i] = output[i]



def bubble_sort(tuples):

    for i in range(0, len(tuples)):
        for j in range(0,len(tuples)-i-1):
            if tuples[j][1] > tuples[j+1][1]:
                 tuples[j], tuples[j+1] = tuples[j+1], tuples[j]




arr = [1,5,2,3,6,2,4,7,5]

tuples = [("george",2),("paul",1),("stefan",5),("ojog",4),("george",17),("paul",17),("stefan",5),("ojog",4),("george",2),("paul",1),("stefan",5),("ojog",4),("george",2),("paul",1),("stefan",5),("ojog",4),("george",2)]

lambda_sort(tuples)
print(tuples)

arr2 = [10,1,5,1,2,6,3,7,4,3,20]


