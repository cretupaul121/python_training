

# prima oara am inteles ca trebuie sa invers cheia cu valoarea:))))
def tupleList(dictionar):
    newdict ={}
    for i in dictionar.keys():
        newdict[dictionar.get(i)] = i

    return newdict.items()

# inversare valori

def reverseList(dictionar):
    i = 1
    j = len(dictionar)

    while i <= j:
        valoare_i = dictionar.get(i)
        valoare_j = dictionar.get(j)

        dictionar[i] = valoare_j
        dictionar[j] = valoare_i
        i += 1
        j -= 1
    return dictionar.items()


d = {
    1: "Gogu",
    2: "Stefan",
    3: "Paul",
    4: "Ionut",
    5: "Nicoara"
}


print(reverseList(d))