


# functie  pentru inlaturarea spatiilor si a caracterelor speciale
def trim(str ):
    s = ""
    for i in str:
        if i not in(" ,.-~@3\"?;:!`'"):
            s += i
    return s

def check_palindrom (str):
    s, d = 0, len(str) -1
    str = str.lower()
    while s < d:
        if str[s] != str[d]:
            return 0
        s += 1
        d -= 1

    return 1


def verifica_palindroame(fisier):

    file = open(fisier, "r")
    lines = file.readlines()
    cnt = 1

    for line in lines:
        line = trim(line.strip())
        checked = check_palindrom(line)
        if checked == 1:
            print("Line " + str(cnt) + " is a palindrom")
        cnt +=1




verifica_palindroame("test.txt")







