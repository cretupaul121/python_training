import math
#extragere numere pare dintr-0 lista
arr = [1,5,2,3,4,6,7,3,2,6,1,2]

arr = list(filter(lambda x : x % 2 == 0, arr))

print(arr)

#extragere numere prime

def isPrim(x):

    for i in range(2,x):
        prim = lambda y : True if y % 2 else  False
        return prim(x)

sir = [1,5,2,3,4,11,1,5,2,2,6,7,3,2,11,14,15]

prime = list(filter(lambda x : isPrim(x), sir))

print(prime)

#extragere caractere a caror cod ascii este impar

str = "am mers la piata si am luat o pisica de gat"

odd = list(filter(lambda x : ord(x) % 2, str))

print(odd)

#citire capete
#citire lista
#calcularea distantei intre cele doua puncte
#verificam daca numarul curent se divide la distanta segmentului dat de capetele introduse

x1 = int(input("Introduceti x1: "))
y1 = int(input("Introduceti y1: "))
x2 = int(input("Introduceti x2: "))
y2 = int(input("Introduceti y2: "))

#ls = []
#
#for i in range(0,n):
#    elem = int(input())
#    ls.append(elem)
#print(ls)

#calculam distanta a doua puncte (x1,y1) (x2,y2)

p1 = [x1, y1]
p2 = [x2, y2]

distance = int(math.sqrt(((p1[0]-p2[0])**2) + ((p1[1] - p2[1])**2)))

ls = [1,7,3,5,6,7,8,9,11]

new_list = list(filter(lambda x : x % distance == 0, set(ls)))

print(distance)
print(new_list)