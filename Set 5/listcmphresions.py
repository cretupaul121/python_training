#toate numerele pare intre 1 si n
import math
n = int(input())

ls_pare = [x for x in range(1,n) if x % 2 ==0 ]

print(ls_pare)

#toate numerele prime intre 1 si n

prime = [x for x in range(1,n) if 0 not in [x % d for d in range(2,x)]]
print(prime)

#array care contine dublurile tuturor numerelor dintr-un sir dat

sir = [1,5,2,3,2,4,3,6,7,8,4,1]

dubluri = [2 * x for x in sir]

print(dubluri)

#array care contine dublurile tuturor numerelor dintr-un fir fara dar fara dublicate

dubluri2 = [x * 2 for x in set(sir)]

print(dubluri2)

#dictionar

def rev(s):
    j = len(s) - 1
    s1 = ""
    while j >= 0:
        s1 += s[j]
        j -= 1
    return s1

def tohexa(nr):
    hexa = ""
    h = "ABCDEF"

    while nr > 0:
        rest = nr % 16
        if rest < 10:
            hexa += str(rest)
        else:
            rest2 = rest % 10
            hexa += h[rest2]
        nr = int(nr / 16)
    return rev(hexa)

def toBin(nr):
    s = ""
    while nr > 0:
        r = nr % 2
        s += str(r)
        nr = int(nr / 2)
    return rev(s)

# testare functie hex cu list comprehesion hexex = [(x,toBin(x)) for x in range(0,100)]


d = {x:(tohexa(x),toBin(x),hash(str(x))) for x in range(0,20)}

print(d)