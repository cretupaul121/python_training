
#inlocuire numar cu patratul lui

arr = [1,6,2,3,6,8,7,1,2,3]

inlocuit = list(map(lambda x : x ** 2, arr))
print(inlocuit)

#inlocuim fiecare element cu valoarea unei functii

functie = lambda x : 2 * x + x**3 - 7

arr2 = [1,5,2,3,6,8,7,5,1,2,3]

result = list(map(functie,arr2))
print(result)

#suma a doua liste

ls1 = [1,2,3,8]
ls2 = [4,7,9]

suma = list(map(lambda x1,x2: x1 + x2, ls1,ls2))

print(suma)