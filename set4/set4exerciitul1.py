
add = lambda x, y : x + y
substract = lambda x, y : x - y
multiply = lambda x, y : x * y
divide = lambda x, y : x / y


d ={}

x1 = int(input("Enter number 1:"))
x2 = int(input("Enter number 2:"))

d["+"] = add(x1, x2)
d["-"] = substract(x1, x2)
d["*"] = multiply(x1, x2)
d["/"] = divide(x1, x2)

for i in d.keys():
    print(d[i])