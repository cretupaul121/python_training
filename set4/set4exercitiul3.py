

set1 = {1,2,2,3,4,5}
set2 = {5,1,7,3,2,7}

dif = set1.difference(set2)
inter = set1.intersection(set2)
uni = set1.union(set2)

print(dif)
print(inter)
print(uni)